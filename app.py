"""
This is the backend API module for the library
"""

import sqlite3
from flask import Flask, request, jsonify


app = Flask(__name__)
DATABASE_NAME = "Library.db"


@app.route("/adduser", methods=["PUT", "POST", "GET"])
def add_user():
    """This method receives a json request and
    creates a new user with data received from the
    request

    Returns:
        json: The JSON response
    """
    try:
        received_request = request.get_json(force=True)
        name = received_request["name"]
        user_name = received_request["username"]
        email = received_request["email"]
        with sqlite3.connect(DATABASE_NAME) as sql_connection:
            cursor = sql_connection.cursor()
            cursor.execute(
                "INSERT INTO users (Name, username, email) VALUES (?,?,?)",
                (name, user_name, email),
            )
            sql_connection.commit()
            display_message = "User added successfully!"
    except Exception as caught_exception:
        sql_connection.rollback()
        display_message = (
            "An error was encountered while performing insert operation.\nError caught:"
            + str(caught_exception)
        )
    finally:
        sql_connection.close()
        response = {"status": display_message}
        return jsonify(response)


@app.route("/checkavailability", methods=["PUT", "POST", "GET"])
def check_availability():
    """This method checks if a certain book is available in
    the database to be issued

    Returns:
        json: The JSON response
    """
    try:
        received_request = request.get_json(force=True)
        book_id = int(received_request["book_id"])
        with sqlite3.connect(DATABASE_NAME) as sql_connection:
            cursor = sql_connection.cursor()
            cursor.execute(f"""SELECT Available FROM books WHERE Book_ID={book_id}""")
            is_available = cursor.fetchall()
            sql_connection.commit()
    except Exception as caught_exception:
        sql_connection.rollback()
        print(
            "An error was encountered while performing SELECT operation.\nError caught:"
            + str(caught_exception)
        )
    finally:
        sql_connection.close()
        response = {"is_available": is_available[0][0]}
        return jsonify(response)


@app.route("/checkbook", methods=["PUT", "POST", "GET"])
def check_book():
    """This method checks if a certain book is available in
    the database

    Returns:
        json: The JSON response
    """
    try:
        received_request = request.get_json(force=True)
        book_name = str(received_request["book_name"])
        is_available = 0
        with sqlite3.connect(DATABASE_NAME) as sql_connection:
            cursor = sql_connection.cursor()
            cursor.execute(f"""SELECT Name FROM books WHERE Name="{book_name}" """)
            name = cursor.fetchall()
            sql_connection.commit()
            if len(name) != 0:
                is_available = 1
    except Exception as caught_exception:
        sql_connection.rollback()
        print(
            "An error was encountered while performing SELECT operation.\nError caught:"
            + str(caught_exception)
        )
    finally:
        sql_connection.close()
        response = {"is_available": is_available}
        return jsonify(response)


@app.route("/checkuser", methods=["PUT", "POST", "GET"])
def check_user():
    """This method checks if the requested user is registered in
    the database

    Returns:
        json: The JSON response
    """
    try:
        received_request = request.get_json(force=True)
        user_name = str(received_request["user_name"])
        is_available = 0
        with sqlite3.connect(DATABASE_NAME) as sql_connection:
            cursor = sql_connection.cursor()
            cursor.execute(f"""SELECT Name FROM users WHERE username= "{user_name}" """)
            name = cursor.fetchall()
            sql_connection.commit()
            if len(name) != 0:
                is_available = 1
    except Exception as caught_exception:
        sql_connection.rollback()
        print(
            "An error was encountered while performing SELECT operation. Error caught:"
            + str(caught_exception)
        )
    finally:
        sql_connection.close()
        response = {"is_available": is_available}
        return jsonify(response)


@app.route("/issuebook", methods=["PUT", "POST", "GET"])
def issue_book():
    """This method issues a book to the requested user

    Returns:
        json: The JSON response
    """
    try:
        received_request = request.get_json(force=True)
        user_name = str(received_request["user_name"])
        book_id = int(received_request["book_id"])
        response_code = 0
        with sqlite3.connect(DATABASE_NAME) as sql_connection:
            cursor = sql_connection.cursor()
            cursor.execute(
                f"""SELECT Books_issued FROM users WHERE username= "{user_name}" """
            )
            books_issued = cursor.fetchall()
            sql_connection.commit()
            if books_issued[0][0] is None:
                cursor.execute(
                    f"""UPDATE users SET Books_issued = "{book_id}" WHERE username= "{user_name}" """
                )
                sql_connection.commit()
                response_code = 1
            elif (
                book_id not in books_issued[0]
                and len(tuple(str(books_issued[0][0]))) < 2
            ):
                if len((books_issued[0])) > 1:
                    books_issued = list(books_issued[0].split(","))
                if book_id not in books_issued:
                    new_list = str(books_issued[0][0]) + "," + str(book_id)
                    cursor.execute(
                        f"""UPDATE users SET Books_issued = "{new_list}" WHERE username= "{user_name}" """
                    )
                    sql_connection.commit()
                    cursor.execute(
                        f"""UPDATE books SET Available = "{0}" WHERE book_id= "{book_id}" """
                    )
                    sql_connection.commit()
                    response_code = 1
    except Exception as caught_exception:
        sql_connection.rollback()
        print(
            "An error was encountered while performing SELECT operation. Error caught:"
            + str(caught_exception)
        )
    finally:
        sql_connection.close()
    response = {"response": response_code}
    return jsonify(response)


@app.route("/returnbook", methods=["PUT", "POST", "GET"])
def return_book():
    """This method returns a book from the requested user

    Returns:
        json: The JSON response
    """
    try:
        received_request = request.get_json(force=True)
        user_name = str(received_request["user_name"])
        book_id = int(received_request["book_id"])
        response_code = 0
        with sqlite3.connect(DATABASE_NAME) as sql_connection:
            cursor = sql_connection.cursor()
            cursor.execute(
                f"""SELECT Books_issued FROM users WHERE username= "{user_name}" """
            )
            books_issued = cursor.fetchall()
            sql_connection.commit()
            if str(book_id) in tuple(books_issued[0][0]):
                issued_list = list(books_issued[0][0].split(","))
                str_book_id = str(book_id)
                issued_list.remove(str_book_id)
                new_list = str(issued_list)[2:-2]
                cursor.execute(
                    f"""UPDATE users SET Books_issued = "{new_list}" WHERE username= "{user_name}" """
                )
                sql_connection.commit()
                cursor.execute(
                    f"""UPDATE books SET Available = "{1}" WHERE book_id= "{book_id}" """
                )
                sql_connection.commit()
                response_code = 1
    except Exception as caught_exception:
        sql_connection.rollback()
        print(
            "An error was encountered while performing SELECT operation. Error caught:"
            + str(caught_exception)
        )
    finally:
        sql_connection.close()
    response = {"response": response_code}
    return jsonify(response)
