**TEAM-21**

Create 2 microservices: 1) This would expose an API layer to end user to achieve some tasks. 2) This would provide APIs to the first service and will communicate with the database

Groups of 4 with 2 teams each of 2:

Requirements:

- Documentation of 4-8 apis of the services.
- Use flask/fast api for your APIs.
- Using proper modules and classes in individual services.
- When both the services are run, the services should be able to communicate with each other.

# **Library API**

**Team members:**

1. Harshil Shah
2. Jay Mangukiya
3. Tirthraj Chaudhari
4. Yash Shribhate

**Task information**

Group-1:(Team members name: Harshil Shah, Jay Mangukiya) Worked on microservice 1

Group-2:(Team members name: Tirthraj Chaudhari, Yash Shribhate ) Worked on microservice 2

**Microservice - 1**

It exposes services like Add User, Add Book, Issue Book, Return Book. And also validates if the user already exists, book already exists, book available for issuing.

**Microservice - 2**

It accepts data from microservice 1 and then sends it to sqlite database and performs operations(adding, retrieving, etc).

Tables are:

- User table which has name, username, email, book\_issued columns.
- Book table which has book name, Author, availability, book\_id columns.

Microservice 1 Routes on 50001 port:

1. localhost:5001/add\_user
2. localhost:5001/add\_book
3. localhost:5001/issue\_book
4. localhost:5001/return\_book

Microservice 2 Routes on 5000 port:

1. localhost:5000/
2. localhost:5000/
3. localhost:5000/
4. localhost:5000/
5. localhost:5000/
6. localhost:5000/